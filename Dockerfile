FROM node:10-alpine

RUN apk update && apk add python git

COPY . ./handbook.uagpmc.com

RUN cd handbook.uagpmc.com && \
      yarn && yarn build && cp -r public-inject/* public/

EXPOSE 80/tcp

CMD cd handbook.uagpmc.com && node_modules/.bin/http-server -p 80 -a 0.0.0.0 --gzip
