<a name="unreleased"></a>
## [Unreleased]


<a name="v1.6.4-pre"></a>
## [v1.6.4-pre] - 2019-03-26
### Merge
- Merge remote-tracking branch 'origin/patch-8'

### Update
- update VERSION and BUILD files [skip ci]

### Updated
- Updated minimum number of operations to attend for new recruits/shinies as of 25/03/2019.


<a name="v1.6.3-pre"></a>
## [v1.6.3-pre] - 2019-03-26

### Update
- update VERSION and BUILD files [skip ci]



<a name="v1.6.2-pre"></a>
## [v1.6.2-pre] - 2019-03-26
### Update
- update VERSION and BUILD files [skip ci]


<a name="v1.6.1-pre"></a>
## [v1.6.1-pre] - 2019-03-26

### Update
- update VERSION and BUILD files [skip ci]



<a name="v1.6.0-pre"></a>
## [v1.6.0-pre] - 2019-03-26
### Merge
- Merge branch 'master' of gitlab.com:zeue.net/uag/handbook

### New
- new cicd config

### Update
- update `bin/tag` for proper tagging increments
- update VERSION and BUILD files [skip ci]

### Update
- Update .gitlab-ci.yml
- Update getting-started.md


<a name="v1.5.3-r2"></a>
## [v1.5.3-r2] - 2019-01-16
### Added
- added infantry-basics to top of sidebar
- added infantry basics by Kris
- added designated marksman by Toby



<a name="v1.5.2-pre"></a>
## [v1.5.2-pre] - 2019-01-13
### I
- I don't like this

### Readd
- readd index/howto page

### Update
- update VERSION and BUILD files [skip ci]



<a name="v1.5.1-pre"></a>
## [v1.5.1-pre] - 2019-01-13
### Update
- update VERSION and BUILD files [skip ci]

### Use
- use __dirname instead of hard directory, remove debug logs


<a name="v1.5.0-pre"></a>
## [v1.5.0-pre] - 2019-01-13

### Big
- big update: sidebar is now dynamic!

### Update
- update VERSION and BUILD files [skip ci]


<a name="v1.4.2-pre"></a>
## [v1.4.2-pre] - 2019-01-08

### Fix
- fix numbering of headers in getting-started.md

### Update
- update VERSION and BUILD files [skip ci]



<a name="v1.4.1-pre"></a>
## [v1.4.1-pre] - 2018-12-31
### I
- I swear I know how to use bash, promise

### Update
- update VERSION and BUILD files [skip ci]


<a name="v1.4.0-pre"></a>
## [v1.4.0-pre] - 2018-12-31

### Badges
- badges update test!

### Update
- update VERSION and BUILD files [skip ci]



<a name="v1.3.2-pre"></a>
## [v1.3.2-pre] - 2018-12-31
### Fix
- fix broken structure in `unit-organisation.md`

### Update
- update VERSION and BUILD files [skip ci]



<a name="v1.3.0-pre"></a>
## [v1.3.0-pre] - 2018-12-31
### Added
- Added grenade use keybinding to Configuration

### Merge
- Merge remote-tracking branch 'origin/patch-3'

### Update
- update VERSION and BUILD files [skip ci]

### Update
- Update antitank-basics.md
- Update antitank-basics.md
- Update antitank-basics.md
- Update antitank-basics.md
- Update antitank-basics.md
- Update unit-organisation.md
- Update antitank-basics.md

### Merge Requests
- Merge branch 'patch-7' into 'master'
- Merge branch 'patch-7' into 'master'


<a name="v1.2.10-pre"></a>
## [v1.2.10-pre] - 2018-12-30

### Add
- add proper header prefix to `autorifleman-basics`'s TL;DR
- add new pages for AT and radio basics

### Add
- Add new file

### Fix
- fix `medical-procedures`'s TL;DR section

### Merge
- Merge branch 'master' of gitlab.com:uag/handbook

### Move
- move work done to unit-organisation under new header

### Prepare
- prepare for AT basics

### Rename
- rename LAT -> AT basics

### Update
- update VERSION and BUILD files [skip ci]

### Update
- Update TheSectionCommander

### Merge Requests
- Merge branch 'patch-3' into 'master'



<a name="v1.2.7-pre"></a>
## [v1.2.7-pre] - 2018-12-30
### Punctuation
- Punctuation updates.

### Update
- update VERSION and BUILD files [skip ci]

### Update
- Update getting-started.md

### Merge Requests
- Merge branch 'patch-1' into 'master'


<a name="v1.2.4-pre"></a>
## [v1.2.4-pre] - 2018-12-30

### Merge
- Merge branch 'radraegon/handbook-patch-1'

### Moved
- Moved "Things to consider before Recruitment" after "Recruitment" and before "Modpack Setup".

### Update
- update VERSION and BUILD files [skip ci]


<a name="v1.2.0-pre"></a>
## [v1.2.0-pre] - 2018-12-30

### Update
- update VERSION and BUILD files [skip ci]



<a name="v1.2.09-pre"></a>
## [v1.2.09-pre] - 2018-12-30
### Fix
- fix gitlab config

### Update
- update VERSION and BUILD files [skip ci]



<a name="v1.1.13-pre"></a>
## [v1.1.13-pre] - 2018-12-30
### Modified
- modified some stuff

### Update
- update VERSION and BUILD files [skip ci]



<a name="v1.1.9-pre"></a>
## [v1.1.9-pre] - 2018-12-30
### Update
- update VERSION and BUILD files [skip ci]


<a name="v1.1.7-pre"></a>
## [v1.1.7-pre] - 2018-12-30

### Update
- update VERSION and BUILD files [skip ci]



<a name="v1.1.5-pre"></a>
## [v1.1.5-pre] - 2018-12-30
### Add
- add new headers for new orbat

### Remove
- Remove ACRE instructions from getting-started.md

### Update
- update VERSION and BUILD files [skip ci]

### Update
- Update `bin/tag` with new changes


<a name="v1.1.2"></a>
## [v1.1.2] - 2018-11-29
### Another
- another prettier run!

### Fix
- fix url resolution issues on refresh

### Prettier
- prettier run!

### Update
- update VERSION file


<a name="v1.0.9"></a>
## [v1.0.9] - 2018-11-29
### Update
- update VERSION file
- update packages


<a name="v1.0.7"></a>
## [v1.0.7] - 2018-11-29
### Replace
- replace unit-org image with new orbat image

### Update
- update VERSION file


<a name="v1.0.4-r5"></a>
## [v1.0.4-r5] - 2018-11-29
### I
- I lied

### Update
- update VERSION file


<a name="v1.0.4-r3"></a>
## [v1.0.4-r3] - 2018-11-29
### Hopefully
- hopefully the last thing I have to do today

### Update
- update VERSION file


<a name="v1.0.4-r2"></a>
## [v1.0.4-r2] - 2018-11-29
### Update
- update VERSION file

### Update
- Update .gitlab-ci.yml 2


<a name="v1.0.4"></a>
## [v1.0.4] - 2018-11-29
### Update
- update VERSION file

### Update
- Update .gitlab-ci.yml


<a name="v1.0.3"></a>
## [v1.0.3] - 2018-11-28
### Add
- add new changelog gen files, delete old changelog file

### Update
- update VERSION file


<a name="v1.0.2-r2"></a>
## [v1.0.2-r2] - 2018-11-28
### Update
- update VERSION file


<a name="v1.0.2"></a>
## [v1.0.2] - 2018-11-28
### Added
- added bin/tag to help automate changelog and release generation

### Update
- update VERSION file
- update code of conduct with first-ever officially-banned meme


<a name="v1.0.0-r7"></a>
## [v1.0.0-r7] - 2018-11-08
### Update
- Update package.json


<a name="v1.0.0-r5"></a>
## [v1.0.0-r5] - 2018-11-08
### Add
- Add README.md
- Add LICENSE

### Add
- add medical* to sidebar
- add recon* page to sidebar
- add initial document framework
- add discord guidelines to code of conduct
- add links to getting-started.md
- add new patch to fix vuepress algolia search
- add patch-package to package.json, regen lock
- add algolia docsearch integration to pages
- add some new chapter directories
- add "get started ->" action button to home

### Change
- change font to fira code instead of roboto mono

### Cleanup
- cleanup getting-started.md

### Delete
- Delete README.md

### Deleted
- Deleted patches/[@vuepress](https://gitlab.com/vuepress)/theme-default+1.0.0-alpha.10.patch

### Fix
- fix titles
- fix some weird shit at the end that was done for no reason >:l
- fix package.json version number
- fix typo in navbar
- fix "get started ->" link

### Get
- get ready for merge

### I
- I done did it. Now kiss me, dad

### Merge
- Merge remote-tracking branch 'origin/5-new-section-uag-organisation'
- Merge branch '3-proofreading-181017'

### Please
- please hurt me

### Remove
- remove "->" from "get started" button

### Rename
- rename file to be consistent, change to Section 3
- rename medical file to be, you know, correct?
- rename recon*.md file to use proper file naming

### Update
- update chapter intro headers
- update fonts to use roboto mono everywhere

### Update
- Update Dockerfile, .gitlab-ci.yml files
- Update .gitlab-ci.yml
- Update .gitlab-ci.yml
- Update .gitlab-ci.yml
- Update docs/ch2/UAV-Operator.md
- Update docs/ch2/UAV-Operator.md
- Update docs/ch2/UAV-Operator.md
- Update docs/ch2/UAV-Operator.md
- Update config.js for autorifleman shit
- Update docs/ch1/Autorifleman_Basics.md
- Update docs/ch1/Autorifleman_Basics.md
- Update docs/ch1/Autorifleman_Basics.md
- Update docs/ch1/Autorifleman_Basics.md
- Update docs/ch1/Autorifleman_Basics.md
- Update docs/ch1/Autorifleman_Basics.md
- Update docs/ch1/Autorifleman_Basics.md
- Update docs/ch1/Autorifleman_Basics.md
- Update reconnaissance-team.md to use proper structure titles
- Update docs/ch1/MedicalProcedures.md
- Update docs/ch1/Autorifleman_Basics.md
- Update docs/ch1/Autorifleman_Basics.md
- Update .gitlab-ci.yml to now patch packages after install
- Update config.js
- Update config.js
- Update getting-started.md
- Update README.md

### Upgrade
- upgrade yarn.lock

### Use
- use my fixed version of vuepress

### When
- when will this end

### Reverts
- use my fixed version of vuepress

### Merge Requests
- Merge branch '8-new-section-uav-operator' into 'master'
- Merge branch '2-new-section-autorifleman-basics' into 'master'
- Merge branch 'master' into '5-new-section-uag-organisation'
- Merge branch '4-new-section-medical-procedures' into 'master'
- Merge branch 'master' into '4-new-section-medical-procedures'
- Merge branch 'Tobuss-master-patch-66483' into 'master'


<a name="1.0.0.0"></a>
## [1.0.0.0] - 2018-10-15
### Add
- Add CHANGELOG
- Add contribution guide

### Add
- add logo to header icon
- add uagLogo to home, update config

### Another
- another big update, fuck off if you want a list

### Big
- big config update, sidebars!

### Enable
- enable cache service worker for book

### Migrate
- migrate to vuepress

### Update
- update vuepress to "next" version

### Update
- Update package.json
- Update .gitlab-ci.yml
- Update README.md


<a name="0.1.0.0-initial"></a>
## 0.1.0.0-initial - 2018-10-15
### Add
- add VERSION file
- add config file, move files to handbook root

### Add
- Add README.md
- Add CHANGELOG
- Add LICENSE file
- Add contribution guide
- Add new file
- Add .gitlab-ci.yml
- Add new file

### Big
- big revert

### Delete
- Delete SUMMARY.md

### Initial
- Initial commit

### Move
- move wiki to gitbook format

### Remove
- remove old TOC syntax

### Update
- Update CONTRIBUTING.md
- Update CONTRIBUTING.md
- Update handbook/README.md
- Update README.md
- Update book.json, handbook/SUMMARY.md, handbook/chapter-1-theBasics/README.md files
- Update SUMMARY.md
- Update README.md
- Update README.md
- Update README.md
- Update README.md

### Update
- update gitlab-ci

### Reverts
- big revert


[Unreleased]: https://gitlab.com/uag/handbook/compare/v1.6.4-pre...master
[v1.6.4-pre]: https://gitlab.com/uag/handbook/compare/v1.6.3-pre...v1.6.4-pre
[v1.6.3-pre]: https://gitlab.com/uag/handbook/compare/v1.6.3-b190326101047...v1.6.3-pre
[v1.6.2-pre]: https://gitlab.com/uag/handbook/compare/v1.6.1-pre...v1.6.2-pre
[v1.6.1-pre]: https://gitlab.com/uag/handbook/compare/v1.6.1-b190326100358...v1.6.1-pre
[v1.6.0-pre]: https://gitlab.com/uag/handbook/compare/v1.5.3-r2...v1.6.0-pre
[v1.5.3-r2]: https://gitlab.com/uag/handbook/compare/v1.5.2-b190113150806...v1.5.3-r2
[v1.5.2-pre]: https://gitlab.com/uag/handbook/compare/v1.5.1-b190113121144...v1.5.2-pre
[v1.5.1-pre]: https://gitlab.com/uag/handbook/compare/v1.5.0-pre...v1.5.1-pre
[v1.5.0-pre]: https://gitlab.com/uag/handbook/compare/v1.5.0-b190113120642...v1.5.0-pre
[v1.4.2-pre]: https://gitlab.com/uag/handbook/compare/v1.4.2-b190108155334...v1.4.2-pre
[v1.4.1-pre]: https://gitlab.com/uag/handbook/compare/v1.4.0-pre...v1.4.1-pre
[v1.4.0-pre]: https://gitlab.com/uag/handbook/compare/v1.4.0-b181231161246...v1.4.0-pre
[v1.3.2-pre]: https://gitlab.com/uag/handbook/compare/v1.3.0-b181231123033...v1.3.2-pre
[v1.3.0-pre]: https://gitlab.com/uag/handbook/compare/v1.2.10-pre...v1.3.0-pre
[v1.2.10-pre]: https://gitlab.com/uag/handbook/compare/v1.2.10-b181230153400...v1.2.10-pre
[v1.2.7-pre]: https://gitlab.com/uag/handbook/compare/v1.2.4-pre...v1.2.7-pre
[v1.2.4-pre]: https://gitlab.com/uag/handbook/compare/v1.2.4-b181230020815...v1.2.4-pre
[v1.2.0-pre]: https://gitlab.com/uag/handbook/compare/v1.2.0-b181230013247...v1.2.0-pre
[v1.2.09-pre]: https://gitlab.com/uag/handbook/compare/v1.1.13-b181230013046...v1.2.09-pre
[v1.1.13-pre]: https://gitlab.com/uag/handbook/compare/v1.1.9-b181230012855...v1.1.13-pre
[v1.1.9-pre]: https://gitlab.com/uag/handbook/compare/v1.1.7-pre...v1.1.9-pre
[v1.1.7-pre]: https://gitlab.com/uag/handbook/compare/v1.1.7-b181230012443...v1.1.7-pre
[v1.1.5-pre]: https://gitlab.com/uag/handbook/compare/v1.1.2...v1.1.5-pre
[v1.1.2]: https://gitlab.com/uag/handbook/compare/v1.0.9...v1.1.2
[v1.0.9]: https://gitlab.com/uag/handbook/compare/v1.0.7...v1.0.9
[v1.0.7]: https://gitlab.com/uag/handbook/compare/v1.0.4-r5...v1.0.7
[v1.0.4-r5]: https://gitlab.com/uag/handbook/compare/v1.0.4-r3...v1.0.4-r5
[v1.0.4-r3]: https://gitlab.com/uag/handbook/compare/v1.0.4-r2...v1.0.4-r3
[v1.0.4-r2]: https://gitlab.com/uag/handbook/compare/v1.0.4...v1.0.4-r2
[v1.0.4]: https://gitlab.com/uag/handbook/compare/v1.0.3...v1.0.4
[v1.0.3]: https://gitlab.com/uag/handbook/compare/v1.0.2-r2...v1.0.3
[v1.0.2-r2]: https://gitlab.com/uag/handbook/compare/v1.0.2...v1.0.2-r2
[v1.0.2]: https://gitlab.com/uag/handbook/compare/v1.0.0-r7...v1.0.2
[v1.0.0-r7]: https://gitlab.com/uag/handbook/compare/v1.0.0-r5...v1.0.0-r7
[v1.0.0-r5]: https://gitlab.com/uag/handbook/compare/1.0.0.0...v1.0.0-r5
[1.0.0.0]: https://gitlab.com/uag/handbook/compare/0.1.0.0-initial...1.0.0.0
