---
home: true
heroImage: /uagLogo.png
actionText: Start Reading
actionLink: /start
---

> This handbook is designed to be used as a learning resource for all things UAG. You can find everything from basic information about the group to advanced leadership strategies and more.
