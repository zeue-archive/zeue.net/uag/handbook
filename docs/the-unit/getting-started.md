# Getting Started

## 1. Recruitment

Join the [Discord](https://uagpmc.com/discord) and ask for a recruiter or message **Cody** `(zeue#0001)`.

## 2. Things to Consider before Recruitment

### 2a. When are our operations?

Our operation times are **Saturday and Sunday at 18:00 UTC** check on the bottom of our [website](https://uagpmc.com/) for a timer till our next operation.

### 2b. Can't Attend a Session?

If you cant attend a session during your time as a recruit please contact one of the Big Boys on discord or steam and let them know 24 hours in advance. Note that currently Zyrtex handles attendance for all members and recruits.

### 2c. Using Premade Loadouts

To use our premade loadouts please refer to [this page](https://armapmc.com/api/get-loadouts/).

### 2d. Restrictions for Recruits

As a Recruit you are just a temporary member within UAG and your role is dynamic. Meaning that you will fill any vital role during an OP that official members have not filled. For instance if an ammo bearer role is not filled by an official member, you will automatically fill said role.

### 2f. Recruitment Process

As a recruit you must attend four operations before we can consider you a full member. We use internal voting to decide whether or not we extend your recruitment, kick you out, or allow you to join based off of your attitude towards other players.

## 3. Modpack Setup

Setting up the modpack is easy, but is quite unique from traditional setups. Because of this, we ask that you follow every step in this guide in order to properly download all of the mods and ensure you can jump into sessions without any issues.

### 3a. Installing

1. Add Cody on [Steam](https://steamcommunity.com/id/codyburton/) to gain access to the private mod.

2. Subscribe to our setup mod on the [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1092924095), this essentially keeps track of all other required mod items and allows for quick install and update.

3. Open up the Arma 3 launcher and select the “MODS” tab and select “Unload all” in the top-right if you have any mods already enabled.

4. Under “Available mods” enable our workshop item and then select the “Load selected mods” option to subscribe to and load our entire modpack.

5. Wait for the launcher to download and enable the mods and then you’re all done!

### 3b. Updating

Updating the modpack is simple! All you need to do is restart your Arma 3 Launcher, unload all mods, then reload the setup mod you downloaded to begin with.

### 3c. Uninstalling

Unload all the mods, select "More" and then select "Unsubscribe From All Mods". If you can't do so then unsubscribe from all the mods manually.

## 4. Setting up Task Force Arrowhead Radio

### 4a. Task Force Arrowhead Radio (TFAR)

To install and set up TFAR just download our modpack once downloaded go to `C:\Program Files (x86)\Steam\steamapps\common\Arma 3\!Workshop\@Task Force Arrowhead Radio (BETA!!!)\teamspeak` and double click the task_force_radio Add-on and it should auto install into your TeamSpeak 3 client once done go to `Tools->Options->Addons` of your TS3 client and enable Task Froce ARMA 3 Radio.

<!--### 3b. Advanced Combat Radio Environment 2 (ACRE2)

Installing and setting up ACRE2 is easy when we use ACRE2 it is automatically installed in our modpack and once you launch the game it will automatically set up in your TeamSpeak 3 client just enable it in Tools-> Options-> Addons of your TS3 client.
-->

## 5. Joining the Server

Before joining the server you are required to do two things:

### 5a. Disable BattlEye

To disable Battleye open your ArmA III launcher, go on BattlEye and untick "Enable BattlEye anti-cheat software"

### 5b. Change your Profile name

You are required to change your ingame profile name to the shortened name assigned to you when you joined the unit. If you are unable to change the profile name, please create a new one with the name assigned. This simply used for identification purposes.

## 6. Configuration

### 6a. Enhanced Movement

In order to use the Enhanced Movement mod i.e climbing over walls or strategically using people as stools, you are required to assign a key of your choice to it. In order to do this go into the 'Options' menu, 'Controls' and then find 'Custom Controls' in the dropdown menu. Find "Use Action 1" and add your preferred key. The recommended key is the backslash '\' button.

### 6b. Grenade Use

It is recommended to set the keybinding for grenades (when throwing them) to "double-tapping" the 'g' key.

## 7. Default Mod Keybindings

To open your Android press 'H'.

To open your Android to interact with it, hold down the 'Left Control' button and press 'H'.

To use Ace Interact, hold down the 'Windows' key.

To use Ace Medical on someone, hold down the 'Windows' key while looking at the person.

To use Ace Medical on yourself, hold down both the 'Left Control' button and the 'Windows' key.
