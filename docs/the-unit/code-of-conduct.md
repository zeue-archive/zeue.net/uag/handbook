# Code of Conduct

The most important thing you must know and agree to in order to be in UAG is our **Code of Conduct**. The rules in this document must be followed by all members of UAG in order to maintain a fun and non-hostile environment for everyone. Failure to follow any of these rules may result in a discussion between all other members regarding the incident, and after a vote is tallied, could potentially end with the termination of a membership with the group.

## 1. Don't be a Cunt

This rule should be pretty straightforward, but some still slip up on this one. We are here to have a good time with friends, not to be bullied harassed by people online. There is a lot of "banter" in the group, as that's the environment we enjoy the most, but sometimes people can take it too far and in those cases we crack down hard on those who ignore multiple warnings to stop.

Here are some brief examples of what we're talking about:

- If you're told to stop, fucking stop. Don't take it as an insult, just drop it and move on.
- Do not bring people's families into things in a malicious manner, keep shit civil.
- Do not make memes of others in poor taste, we have a lot of inside jokes but doing it for insidious reasons is just fucked.
- Doing shit for the sole purpose of "being edgy" is just stupid, stop it, get some help.

## 2. Buddy Teams

When playing in-game we assign people together in teams of two, usually in complimentary pairs to increase combat effectiveness. We require that all players stick with their buddy no matter what, this allows us to maintain a much more firm condition of situational awareness and saves contractors' lives every session. Your buddy is your responsibility, and your buddy team itself is the responsibility of the team leader. Never give them up, never let them down, never desert them.

## 3. Discord Guidelines

We use Discord outside of sessions to coordinate and communicate with everyone much easier than the Steam or TeamSpeak-only alternatives. By using Discord, you already agree to following the [guidelines](https://discordapp.com/guidelines), so as a unit hosted primarily on Discord we expect everyone to follow these guidelines by default.

## 4. Banned Memes

In order to maintain the sanity of all that shitpost in the UAG Discord, we have some **banned memes** that all users are not permitted to post in any text channel. DMs are non-policable, this only applies to the Official Discord's channels!

The banned memes, and all other sub-meme variants of said memes, are as follows:

- "They did surgery on a grape"

## 5. Don't Listen to Collin

Seriously, he's fucking retarded. Why do you guys keep listening to him like he even has a right to an opinion?
