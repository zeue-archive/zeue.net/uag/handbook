# Introduction

> This chapter will help new recruits get a grip of how UAG is organised and understand the different systems and protocols in place within the group.
