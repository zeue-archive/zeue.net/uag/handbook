# Unit Organisation

## 1. Order of Battle (ORBAT)

[![](/orbat-comms.png)](/orbat-comms.png)

> UAG's structure may change during sessions to fit the current situation better, but the general structure supports our focus on "small team tactics" no matter how many people are attending a session.

### 1a. Crossroads

`...`

#### Banana Boys

`...`

### 1b. Wolfpack

`...`

#### Base-of-Fire Team

`...`

#### Assault Team

`...`

### 1c. Talon

`...`

#### Team One

`...`

#### Team Two

`...`

### 1d. Armadillo

`...`

### 1e. Hammer

`...`

#### Forward Observation

`...`

### 1f. Recon Team

`...`

#### Remote Surveillance Team

`...`

#### Force Recon Team

`...`

#### Sniper Team

`...`

### 1g. Air Corps

`...`

## 2. Non-Combat Groups (NCGs)

`...`

### 2a. Administration

`...`

### 2b. Media

`...`

### 2c. Recruitment

`...`

### 2d. Technology

`...`

### 2e. Intelligence

`...`

## 3. Leadership Roles ("Ranks")

### 3a. Section Commander

The role of the Section Commander is to streamline communication between the Team Leaders of the section with High Command through a command chain as displayed in the UAG Orbat at the top of this page. Furthermore the Section Command is there to coordinate all teams of the section towards the objective as strategically as possible while ensuring the flow of information between all teams is as clear as possible.

The main goals of the Section Commander in UAG are as follows:

1. The Section Commander must ensure that the flow of information between High Command and the section is clear as possible.

2. The Section Commander must ensure that all teams are alive and well and knows their exact location through the constant use of the Android provided.

3. The Section Commander must ensure that the teams are pursuing their objectives and following orders.

### 3b. The Team Leader

The role of the Team Leader is to streamline communication between the team members and Section Command through a command chain as displayed in the UAG Orbat at the top of this page. Furthermore the Team Lead is there to guide the team members towards the objective as strategically as possible while ensuring their survival.

The main goals of the Team Leader in UAG are as follows:

1. The Team Lead must ensure that the flow of information between Section Command and the team members is clear as possible.

2. The Team Lead must ensure that all members are alive and well and knows their exact position through the constant use of the Android provided.

3. The Team Lead must ensure that the team assigned is doing its objective/s as effectively and diligently as possible.

4. The Team Lead must ensure that all members of the team are enjoying their time and not finding it tedious due to them dying several times in a row.

## 4. Radio Communication

The team leader's main tool for communication with both of his team and other team leaders as well as with Section Command is the 152 Radio.

### 4a. How to use the 152 radio

To use the 152 radio hold down the 'Left Control' button and press 'P'. This will bring up the radio interface. The recommended setup is as follows:
Say for instance you are in Wolfpack, the radio setup for Wolfpack is 31. Hence the setup should be as follows:

- Channel 1: 31.1 (Blue Team)

- Channel 2: 31.2 (Red Team)

- Channel 3: 31.3 (Green Team)

- Channel 4: 31 (Set as additional channel with the right arrow key displayed on the interface and set as speaker with the left arrow key.)

To use the additional channel and your main channel as detailed above, make sure that 'Channel 4' is set as 'Additional Channel 4' and switch to the channel of your respective team by having it displayed on the radio interface or by using the numpad, 1, 2 or 3 respectively. In order to communicate between the two channels, hold down the 'Caps Lock' button to communicate with your respective team and hold down the letter 'T' button to communicate with Section Command and other team leads. It is important to not switch hastily between the two channels due to a small limitation within the mod. If you do so, whatever you transmit will be most likely transmitted to the wrong channel.

Due to 'Channel 4' being set as 'Additional Channel 4', you will be able to hear both your team's chatter as well as the chatter of section command. Radio communication may be confusing at first due to several people chattering at the same time, but you will get a hang of it and learn who is who. In case you cannot hear what Section Command is saying due to unnecessary chatter, go on your respective team's radio by holding the 'Caps Lock' button and say "Clear Comms". Your team members should know this term and will not use the radio until told otherwise or when transmitting important information.

It is important to know what section and team you are in, in order to prevent confusion and miscommunication.

### 4b. When to use the 152 radio

The 152 radio is used by the team lead to coordinate the members of the team. It is also used to send information and recieve orders to and from Section Command. Use the 152 radio to call out the status of the team, for instance, if you are under fire you are to call it out to the Section Commander by saying "Contact". A full list of Radio Terminology is found [here].

### 4c. Strategy

While pursuing the objective the Team Lead must understand basic formations, such as the Wedge and Column formations. The Team Lead must also understand when and where to use them given a particular situation.

Furthermore while pursuing the objective, the Team Lead must keep several things in check:

1. Spacing - The Team Lead must ensure that enough spacing is made between members of the team. This will increase the chances of a member surviving should the team be attacked.

2. Blobbing - The Team Lead must ensure that no members are grouping together regardless of the situation, be it Urban or Trench warfare. If you, as a team leader don't want to lead soldiers with no limbs, ensure that this doesn't happen.

3. Guns Are Up - During combat its a given that some members will go down. Call it out on the radio and have a team member attempt to take care of the wounded soldier. However if you as a team are still under fire, do not have or let any soldier within your team take care of the wounded. Keep their guns up and firing, call out the wounded on 31 - The Section Medic will then take care of the rest.

4. Buddy Teams - One of the Golden Rules of UAG. When separating someone from the team, ensure that they have another member within your team with them to ensure that they survive any encounter and you recieve comms if one of them goes down.

### 4d. Initiative

It is important for the Team Lead to have initiative when pursuing the objective during an OP. Meaning that the Team Lead must have a clear strategy in mind while pursuing the given objective. In most cases you are to always think two steps ahead and having a fallback plan in place should things go awry. Initiative is also key when taking any actions necessary should things go wrong or not as planned.

For instance, if you are Blue Team Leader and both Red Team Lead and Section Command are down while under fire, take point and coordinate all remaining members of the teams as necessary for the situation through the radio channels. During such a situation make it your highest priority to tend to the wounds of the Section Commander while keeping yourself alive. The reason why you should remain alive is that you will be the only connection to the rest of the teams should High Command attempt to contact you through 31. I emphasize once more, Initiative is the key for every Team Leader of UAG in the field.
