# Handbook Start

Welcome to the official UAG handbook! Here you will find everything UAG-related that we could be bothered to write down.

The handbook is organised into two main parts:

- ["The Unit"](/the-unit/): **New to the unit?** Want to learn more about how we're structured and what we actually do here? This is the section for you!
- ["TTP"](/ttp/): This stands for **Tactics, Techniques, and Procedures** which is a phrase pulled from Dslyecxi's Arma guides of the same namesake. This section of the handbook is dedicated to "technical" write-ups of general combat skills and specific-case protocols.

Oh, and if you see anything wrong with this handbook, feel free to correct it! At the bottom of every page on this site is a link to the GitLab repository where all of the source code is open and available to improve.
